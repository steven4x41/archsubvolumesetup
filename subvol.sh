#!/bin/bash

# Set keyboard layout
loadkeys YOUR_KEYBOARD_LAYOUT

# Connect to the internet (if using Wi-Fi, use 'wifi-menu' or 'iwctl')
ping -c 3 google.com

# Update system clock
timedatectl set-ntp true

# Partitioning (assuming /dev/sda as the drive)
# Example: Create a single partition and format it
# Use 'fdisk' or 'cfdisk' for interactive partitioning
# Replace /dev/sda1 with your desired partition
# mkfs.ext4 /dev/sda1

# Mount the root partition
# mount /dev/sda1 /mnt

# Install base system
pacstrap /mnt base linux linux-firmware

# Generate filesystem table
genfstab -U /mnt >> /mnt/etc/fstab

# Change root into the new system
arch-chroot /mnt

# Set timezone
ln -sf /usr/share/zoneinfo/Your_Region/Your_City /etc/localtime
hwclock --systohc

# Uncomment needed locales in /etc/locale.gen and generate locales
# echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
# locale-gen
# echo "LANG=en_GB.UTF-8" > /etc/locale.conf

# Set hostname
# echo "YourHostname" > /etc/hostname

# Set root password
# passwd

# Install and configure bootloader (GRUB or other)
# Install GRUB: pacman -S grub
# Configure GRUB: grub-install /dev/sda && grub-mkconfig -o /boot/grub/grub.cfg

# Exit chroot, unmount partitions, and reboot
# exit
 #umount -R /mnt
 #reboot

